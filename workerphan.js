const phantom = require('phantom');
var sleep = require('system-sleep');
const proxy = require('./proxy.js');
const {
    workerData: {
        start,
        end,
        link
    }
} = require('worker_threads');

(async function() {
  for(let k=start;k<end;k++){  
    for(let i=0;i<2;i++){
        try{
        console.log('Process triggered.');
        console.time('Execution time');
      const instance = await phantom.create([`--proxy=${proxy.proxy[k]}`,'--ignore-ssl-errors=yes', '--load-images=no']);
      const page = await instance.createPage();
      await page.on('onResourceRequested', function(requestData) {
      });
      await page.open(link);
      //sleep(3000);
      console.log('Process end.');
      console.timeEnd('Execution time');
    }catch(e){};
 }
}})();
 